#!/usr/bin/python
"""Module for analyzing data from the Liverpool telescope"""

try:
    from astropy.io import fits
except ImportError:
    import pyfits as fits
from os.path import expanduser, join, basename, splitext
import numpy as np
import matplotlib.pyplot as plt
# import aplpy
import pydis

# path to data files
datadir = expanduser('~/project/67P/LT/')
SPRAT_dir = join(datadir, 'SPRAT/')

class log(object):
    """class to read the data logs"""
    pass

class SPRAT_data(object):
    """SPRAT comet observations data"""
    # comet data tuple has:
    # 10 s exposures, 3x300 exposures, 10s arc
    comet = [ ['v_a_20151116_27_1_0_1', 'v_e_20151116_26_[1-3]_0_2',
                   'v_q_20151116_2[1-5]_1_0_1'],
              ['v_a_20151106_27_1_0_1', 'v_e_20151106_26_[1-3]_0_2',
                   'v_q_20151106_2[2-5]_1_0_1'],
              ['v_a_20151027_68_1_0_1', 'v_e_20151027_67_[1-3]_0_2',
                   'v_q_20151027_6[3-6]_1_0_1'],
              ['', 'v_e_20151010_13_[1-3]_0_2', ''],
              ['', 'v_e_20150930_17_[1-3]_0_2', ''],
              ['', 'v_e_20150913_18_[1-3]_0_2', ''],
              ['', 'v_e_20150903_36_[1-3]_0_2', ''],
            ]
    # prepend datadir and append fits extension to all file names
    # use numpy'as in chararray subclass for nested lists 
    comet = np.core.defchararray.add(SPRAT_dir, np.array(comet))
    comet = np.core.defchararray.add(comet, '.fits').tolist()

    # standard stars from the SPRATstand proposal (public data)
    standard = [ 'v_s_20151128_13_1_0_2', 'v_s_20151126_5_1_0_2',
            'v_s_20151123_12_1_0_2', 'v_s_20151121_8_1_0_2',
            'v_s_20151120_13_1_0_2', 'v_s_20151117_14_1_0_2',
            'v_s_20151116_12_1_0_2',
            # this image only has one FITS extension
            # 'v_e_20151028_52_1_0_1',
            ]
    standard = [join(SPRAT_dir, i+'.fits') for i in standard]

    # solar analog observed by IL15B01a proposal
    solar_analog = [ 'v_e_20150903_14_1_0_2', 'v_e_20150903_13_1_0_2',
            ]
            # these are not target images
            # 'v_e_20150903_12_1_0_1', 'v_q_20150903_11_1_0_1',
            # 'v_q_20150903_10_1_0_1',
    solar_analog = [join(SPRAT_dir, i+'.fits') for i in solar_analog]


class LOTUS_data(object):
    """LOTUS comet observations data"""
    # Define 67P data that can be co-added using python glob
    comet = [ 'l_e_20150904_2_[1-3]_1_2', 'l_e_20150909_2_[1-3]_1_2',
            'l_e_20150914_6_[1-3]_1_2', 'l_e_20150929_2_[1-3]_1_2',
            'l_e_20151006_1_[1-3]_1_2', 'l_e_20151012_1_[1-3]_1_2',
            'l_e_20151025_1_[1-3]_1_2', 'l_e_20151102_2_[1-3]_1_2',
            'l_e_20151107_2_[1-3]_1_2', 'l_e_20151109_1_[1-3]_1_2',
            'l_e_20151111_1_[1-3]_1_2', 'l_e_20151113_1_[1-3]_1_2',
            'l_e_20151202_1_[1-3]_1_2', 'l_e_20151209_2_[1-6]_1_2',
            ]
    comet_background = [ 'l_e_20150904_[3-5]_1_1_2', 'l_e_20150909_[3-5]_1_1_2',
            'l_e_20150914_7_1_1_2', 'l_e_20150929_3_1_1_2',
            'l_e_20151006_2_1_1_2', 'l_e_20151012_2_1_1_2',
            'l_e_20151025_2_1_1_2', 'l_e_20151102_3_1_1_2',
            'l_e_20151107_3_1_1_2', 'l_e_20151109_2_1_1_2',
            # backgfound from 20151111 not available
            'l_e_20151113_2_1_1_2', 'l_e_20151113_2_1_1_2',
            'l_e_20151202_2_1_1_2', 'l_e_20151209_3_1_1_2',
            ]
    # clouded observations
    comet_cloud = [ 'l_e_20151118_2_[1-3]_1_2' ]
    comet_cloud_background = [ 'l_e_20151118_3_1_1_2' ]

    # standard stars
    standard = [ 'l_e_20150929_1_1_1_1', 'l_e_20150930_2_1_1_1',
            'l_e_20151026_2_1_1_2', 'l_e_20151028_1_1_1_2',
            'l_e_20151029_1_1_1_2', 'l_e_20151102_1_1_1_2',
            'l_e_20151103_1_1_1_2', 'l_e_20151104_1_1_1_2',
            'l_e_20151105_2_1_1_2', 'l_e_20151106_1_1_1_2',
            'l_e_20151107_1_1_1_2', 'l_e_20151108_1_1_1_2',
            'l_e_20151115_2_1_1_2', 'l_e_20151116_1_1_1_2',
            'l_e_20151117_1_1_1_2', 'l_e_20151118_1_1_1_2',
            'l_e_20151120_1_1_1_2', 'l_e_20151121_1_1_1_2',
            'l_e_20151123_1_1_1_2', 'l_e_20151126_1_1_1_2',
            'l_e_20151126_2_1_1_1', 'l_e_20151126_3_1_1_2',
            'l_e_20151126_4_1_1_1', 'l_e_20151126_5_1_1_2',
            'l_e_20151126_6_1_1_1', 'l_e_20151126_7_1_1_2',
            'l_e_20151126_8_1_1_1',
            ]
    # prepend datadir and append fits extension to all file names using the
    # locals dictionary
    for v in ['comet', 'comet_background', 'comet_cloud',
            'comet_cloud_background', 'standard']:
        locals()[v] = [join(datadir, 'LOTUS', i+'.fits') for i in locals()[v]]

class LOTUS_FITS(object):
    """Class for a FITS LOTUS file"""

    def __init__(self, filename):
        """Read data from FITS file
        """
        self.filename = filename
        self.hdus = fits.open(filename)
        self.airmass = self.hdus[0].header['AIRMASS']
        # correct 2d spectrum (extension 1)
        self.spec2d = self.hdus[1].data/self.hdus[1].header['EXPTIME']

class LTspec(object):
    """Class to work with spectra from Liverpool telescope"""

    def __init__(self, filename, lofreq=None,
                airmass_file='ormextinct.dat'):
        """Read data from FITS file

        Parameters
        ----------
        filename : string, sequence
            FITS file(s)
        sky : LOTUS_object
            sky spectrum object
        lofreq : array
            lower value to clip the frequency and data arrays
        """
        if isinstance(filename, list):
            self.filename = filename[0]
            # If a list of files is provided
            data_list = []
            for f in filename:
                hdus = fits.open(f)
                airmass = hdus[0].header['AIRMASS']
                spec2d = hdus[1].data/hdus[1].header['EXPTIME']
                self.freq = self.get_wl(hdus[1])
                # correct the object flux for airmass extinction
                spec2d = pydis.AirmassCor(self.freq, spec2d, airmass,
                    airmass_file=airmass_file)
                data_list.append(spec2d)
            # co-add data
            self.spec2d = np.median(np.dstack(data_list), axis=2)
            self.hdus = hdus
        else:
            self.filename = filename
            self.hdus = fits.open(filename)
            airmass = self.hdus[0].header['AIRMASS']
            # correct 2d spectrum (extension 1)
            spec2d = self.hdus[1].data/self.hdus[1].header['EXPTIME']
            self.freq = self.get_wl(self.hdus[1])
            # correct the object flux for airmass extinction
            self.spec2d = pydis.AirmassCor(self.freq, spec2d, airmass,
                airmass_file=airmass_file)

        if lofreq:
            # apply mask to arrays
            mask = np.where(self.freq > lofreq)
            idx = mask[0][0]
            self.freq = self.freq[mask]
            if hasattr(self, 'flux'):
                self.flux = self.flux[mask]
            self.spec2d = self.spec2d[:,idx:]
        self.basename = splitext(basename(self.filename))[0]

    def extract(self, ap=6, skysep=10, skywidth=7, skydeg=0, subtract=False,
            trace_display=False):
        """Extract 1D spectrum with pydis

        Parameters
        ----------
        ap : integer
            aperture semi-width centered on the trace
        """
        self.trace = pydis.ap_trace(self.spec2d, nsteps=7, interac=False,
                        display=trace_display)
        self.flux, self.sky, self.fluxerr = pydis.ap_extract(self.spec2d,
                        self.trace, apwidth=ap, skysep=skysep,
                        skywidth=skywidth, skydeg=skydeg)
        if subtract: self.flux -= self.sky

    def fluxcal(self, stand_spec):
        self.ffinal, self.efinal = pydis.ApplyFluxCal(self.freq, self.flux,
                        self.fluxerr, stand_spec.freq, stand_spec.sensfunc)

    def print(self):
        """Print FITS information"""
        print(self.hdus.info())

    def get_wl(self, hdu):
        """calculate frequency scale

        Parameters
        ----------
        hdu : header data unit
            header data unit

        Output
        ------
        freq : array
            frequency array
        """
        x = hdu.header['NAXIS1']
        self.crval1 = hdu.header['CRVAL1']
        self.cdelt1 = hdu.header['CDELT1']
        self.crpix1 = hdu.header['CRPIX1']
        freq = self.crval1 + self.cdelt1*(np.arange(1, x+1) - self.crpix1)
        return freq

    def get_pixel(self, freq):
        """Get pixel number for a given frequency
        """
        return (freq - self.crval1) / self.cdelt1 + self.crpix1

    def plot_ext(self):
        """Plot all the extensions in the FITS file
        """
        # gc = aplpy.FITSFigure(self.hdus[0])
        # gc.show_colorscale(stretch='arcsinh')

        # gc = aplpy.FITSFigure(self.spec2d)
        # gc.show_colorscale()
        plt.imshow(np.log10(self.spec2d), origin='lower',
                    extent=[self.freq[0], self.freq[-1], 0, 207],
                    aspect='auto')
        plt.show()
        # gc.show_colorscale(stretch='arcsinh')

        plt.plot(self.freq, self.flux)
        # plt.plot(self.freq, self.hdus[3].data[0])
        plt.show()

    def __arcsinh(self, x):
        """scaling function from aplpy"""
        m = -1./30
        return np.arcsinh(x/m)/np.arcsinh(1/m)

    def quick_plot(self, flux_list=[]):
        """plot including 2D and 1D spectra"""
        f, axarr = plt.subplots(3, sharex=True)
        axarr[2].imshow(np.log10(self.spec2d), origin='lower',
                extent=[self.freq[0], self.freq[-1], 0, 207],
                aspect='auto')
        axarr[2].autoscale(False)
        # axarr[0].plot(self.freq, self.hdus[2].data[0])
        axarr[1].plot(self.freq, self.flux, label='object')
        axarr[1].plot(self.freq, self.sky, label='sky')
        axarr[1].legend()
        for fl in flux_list:
            axarr[1].plot(self.freq, fl)
        axarr[0].plot(self.freq, self.ffinal)
        # Set axis limits of subplot
        # axarr[0].set_xlim(self.freq[0], self.freq[-1])
        yticks = axarr[0].yaxis.get_major_ticks()
        yticks[0].label1.set_visible(False)
        # axarr[0].set_title(self.hdus[0].header['DATE-OBS'].replace('T', ' '))
        # Fine-tune figure; make subplots close to each other and hide x ticks
        # for all but bottom plot.
        f.subplots_adjust(hspace=0)
        plt.setp(axarr[1].get_xticklabels(), visible=False)
        axarr[2].set_xlabel(r'Wavelength (Angstroms)')
        axarr[2].set_ylabel(r'Pixel')
        axarr[1].set_ylabel(r'Flux (ADU s$^{-1}$)')
        axarr[0].set_ylabel(r'Flux (erg/s/cm$^2$/$\AA$)')
        plt.savefig(join(datadir, 'figures',
                '{0}_fluxcal.png'.format(splitext(basename(self.filename))[0])))

    def slice_plot(self, slice_px, trace=0, ap=0, skysep=0, skywidth=0):
        """Plot slice in the 2d spectrum at a given wavelength

        Parameters
        ----------
        slice_px : int
            slice pixel along the slit
        trace : int
            optocenter pixel
        ap : int
            semi-width of the aperture around the optocenter
        """
        plt.plot(self.spec2d[:, slice_px])
        if trace and ap:
            plt.axvspan(trace - ap, trace + ap, alpha=0.5, color='y', lw=0)
            if skysep and skywidth:
                x = trace - ap - skysep
                plt.axvspan(x - skywidth, x, alpha=0.5, color='b', lw=0)
                x = trace + ap + skysep
                plt.axvspan(x, x + skywidth, alpha=0.5, color='b', lw=0)
        plt.show()

    def plot(self):
        """Plot flux corrected spectrum"""
        plt.plot(self.freq, self.ffinal, label=basename(self.filename))
        plt.ylabel('Flux (erg/s/cm$^2$/$\AA$)')
        plt.xlabel(r'Wavelength ($\AA$)')
        plt.legend()
        plt.ylim(ymin=0)
        plt.savefig(join(datadir, 'figures',
                '{0}.png'.format(splitext(basename(self.filename))[0])))
        plt.close()

    def write_fits(self):
        """Write flux to FITS file"""
        fits_file = join(datadir, 'fits', '{0}.fits'.format(self.basename))
        cols = [fits.Column(name='wavelength', format='E', array=self.freq)]
        if hasattr(self, 'flux'):
            cols.append(fits.Column(name='flux', format='E', array=self.flux))
        if hasattr(self, 'ffinal'):
            cols.append(fits.Column(name='calibrated flux', format='E', array=self.flux))
        tbhdu = fits.BinTableHDU.from_columns(cols)
        for k in self.hdus[1].header:
            tbhdu.header.append((k, self.hdus[1].header[k], ''))
        tbhdu.writeto(fits_file)

    def write_ascii(self):
        """Write flux to ASCII file"""
        import pandas as pd
        from collections import OrderedDict
        out_file = join(datadir, 'fits', '{0}.txt'.format(self.basename))
        df = pd.DataFrame.from_dict(OrderedDict((('wavelength', self.freq),
                                    ('flux', self.flux),
                                    ('calibrated flux', self.ffinal))))
        df.to_csv(out_file, index=False)

class SPRAT_spec(LTspec):
    """Class to work with spectra from SPRAT instrument"""
    pass

class LOTUS_spec(LTspec):
    """Class to work with spectra from LOTUS instrument"""
    pass

class LOTUS_object(LOTUS_spec):
    """Class to create a LOTUS object spectra"""

    def __init__(self, *args, sky=None, **kwargs):
        """
        Parameters
        ----------
        stand_spec: LOTUS_stand object
            LOTUS_stand object of a standard star. If this argument is defined
            flux calibration is applied
        """
        super(LOTUS_object, self).__init__(*args, **kwargs)
        if sky:
            self.spec2d -= sky.spec2d

    def quick_plot(self, **kwargs):
        super(LOTUS_object, self).quick_plot(**kwargs)
        axarr[0].set_xlim(3300, self.freq[-1])
        axarr[0].set_ylim(-5, 30)

class LOTUS_stand(LOTUS_spec):
    """Class to create a LOTUS standard spectra"""

    def __init__(self, *args, stdstar="", **kwargs):
        """
        Parameters
        ----------
        name : string
            name of standard star (case insensitive)
        """
        super(LOTUS_stand, self).__init__(*args, **kwargs)
        if not stdstar:
            self.stdstar = 'spec50cal/{}.dat'.format(
                            self.hdus[0].header['OBJECT'])

    def extract(self, display=False, mode='spline', **kwargs):
        super(LOTUS_stand, self).extract(**kwargs)
        self.sensfunc = pydis.DefFluxCal(self.freq, self.flux,
                        mode=mode, stdstar=self.stdstar, display=display)

    def fluxcal(self):
        """Apply flux calibration"""
        self.ffinal, self.efinal = pydis.ApplyFluxCal(self.freq, self.flux,
                        self.fluxerr, self.freq, self.sensfunc)

class SPRAT_object(LTspec):
    """Class to work with spectra from SPRAT instrument"""

    # def __init__(self, *args, **kwargs):
    #     """
    #     Parameters
    #     ----------
    #     name : string
    #         name of standard star (case insensitive)
    #     """
    #     super(LTspec, self).__init__(*args, **kwargs)
    #     # limit to 203 pixels along the slit to work for first two
    #     # nights
    #     # [:203,:]

class SPRAT_stand(LOTUS_stand):
    """Class to work with spectra from SPRAT instrument"""
    pass


if __name__ == "__main__":
    import argparse
    import glob

    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--inst', help='instrument name',
                    default='LOTUS')
    parser.add_argument('-f', '--filename', help='data file name',
                    default='l_e_20151118*')
    parser.add_argument('-s', '--standard', help='standard file name',
                    default='l_e_20151126_7_1_1_2.fits')
    args = parser.parse_args()

    # standard star
    stand = LTspec(join(datadir, args.inst, args.standard), std='G191B2B')

    comet_fits = glob.glob(join(datadir, args.inst, args.filename))
    for filename in comet_fits:
        print(filename)
        spec = LTspec(filename, stand_spec=stand)
    spec.plot()
