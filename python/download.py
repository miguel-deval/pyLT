#!/usr/bin/python

import requests
from os.path import join

scratch = 179081907
url = "http://telescope.astro.livjm.ac.uk/data/archive/scratch/{}/IL15B01a/Priv/".format(scratch)
date = '20151106'

for l in ('a', 'e', 'q'):
    for i in range(22, 28):
        for j in range(1, 4):
            for k in range(1, 3):
                filename = 'v_{}_{}_{}_{}_0_{}.fits'.format(l, date, i, j, k)
                r = requests.get(join(url, filename), stream=True,
                        auth=('IL15B01a', '4671241'))

                if r.status_code == 200:
                    print(filename)
                    # stream data to a file
                    with open(filename, 'wb') as fd:
                        for chunk in r.iter_content(chunk_size=1024):
                            fd.write(chunk)
