#!/usr/bin/python

from os.path import join, basename, splitext, expanduser
from spiceypy import wrapper
from liverpool import LOTUS_object, LOTUS_data, LOTUS_stand, LOTUS_spec, datadir
import pandas as pd
import matplotlib.pyplot as plt
import glob
import pydis
import numpy as np
from astropy.io import fits as pyfits
from scipy.integrate import simps, dblquad
from scipy.constants import constants
from scipy.interpolate import interp2d
from qcomet import qcomet
import sqlite3
from datetime import datetime, timedelta

def equiv_radius(length, width=2.5):
    """
    Calculate equivalent radius for a rectangular slit to be used in a Haser
    model

    Parameters
    ----------
    length : float
        apertura in arcsec (number of pixels*arcsec/pixel)
    width : float
        slit width in arcsec
    Returns
    -------
    equiv_radius : float
        equivalent radius
    """
    x = np.sqrt(length*length+width*width)
    y = length * np.log(x + width/length)
    z = width * np.log(x + length/width)

    equiv_radius = (y+z)/np.pi

    return equiv_radius

def quick_view(target='67P'):
    """Read data from log file"""
    df = pd.read_csv('lotus_log.txt', sep="\s*", engine='python')

    # Create quick view figures
    for f in df[df['Name'].str.contains(target)]['file'][::-1]:
        filename = join(data.datadir, 'LOTUS', '{0}.fits'.format(f))
        print(filename)
        hdus = pyfits.open(filename)
        # print(hdus.info())
        # plt.imshow(hdus[0].data, origin='lower')
        # plt.title(len(hdus))
        # plt.show()
        if len(hdus) == 4:
            spec = LOTUS_object(filename)
            spec.print()
            spec.quick_plot()

def background(freq, flux):
    """
    Parameters
    ----------
    freq : array
        in A
    flux : array
        in erg/s/cm2/A

    Returns
    -------
    area : array
        integrated band flux
    """
    cnfreq = 3883
    shift = 10
    cnfreq -= shift
    width = 40
    plt.plot(freq, flux)
    plt.axvline(cnfreq)
    back = [np.logical_or(cnfreq - width > freq, freq > cnfreq + width)][0]
    plt.axvspan(cnfreq - width, cnfreq + width, alpha=0.5, color='y', lw=0)
    # Fit a 2nd order polynomial to continuum
    pfit = np.polyfit(freq[back], flux[back], 2)
    # Evaluate the polynomial at x
    yfit = np.polyval(pfit, freq)
    plt.plot(freq, yfit)
    plt.ylabel('Flux (erg/s/cm$^2$/$\AA$)')
    plt.xlabel(r'Wavelength ($\AA$)')
    plt.title(spec.hdus[0].header['DATE-OBS'].replace('T', ' '))
    plt.savefig(join(datadir, 'figures',
            'CN_{0}.png'.format(splitext(basename(files[0]))[0])))
    plt.close()
    # line intensity in erg/s/cm2
    area = simps((flux - yfit)[~back], freq[~back])
    return area

# quick_view('G191')

data = LOTUS_data()

# SPICE kernels to calculate rh, rhdot and delta
kernels = [expanduser('~/data/kernels/planets.bsp'),
            expanduser('~/data/kernels/pck.tpc'),
            expanduser('~/data/kernels/naif.tls'),
            expanduser('~/project/67P/1000012.bsp'),
            ]

wrapper.furnsh(kernels)

pixel = 0.6 # arcsec
# g-factor tables from Schleicher 2010
z = np.loadtxt('cn.dat')
# rh in AU
x = np.array([0.250, 0.354, 0.500, 0.707, 1.000, 1.414, 2.000, 2.828, 4.000,
    5.657])
# rhdot in km/s
y = np.arange(0, 26)
f = interp2d(x, y, z)

# standard star
stand = LOTUS_stand(data.standard[4])
stand.extract()

# Find trace for spectra from first night
files = glob.glob(data.comet[0])
spec = LOTUS_object(files)
trace = pydis.ap_trace(spec.spec2d[50:], nsteps=7, interac=False, display=False)

date = []
q_list = []
for i, wc in enumerate(data.comet):
    # exposures from same night that can be added together
    files = glob.glob(wc)
    # use first sky bakckround image
    bk_file = glob.glob(data.comet_background[i])[0]
    print(basename(files[0]))
    spec = LOTUS_object(files, sky=LOTUS_spec(bk_file))
 
    # calculate ephemeris using kernel from JPL Horizons
    tbd = wrapper.str2et(spec.hdus[0].header['DATE-OBS'])

    state = wrapper.spkezr('1000012', tbd, 'J2000', 'LT+S', '10')
    rh = np.linalg.norm(state[0][:3]) # km
    # dot product to calculate velocity rhdot = np.dot(v, rh)/|rh| in km/s
    rhdot = np.dot(state[0][3:6], state[0][:3])/rh # km/s
    rh *= 1e3 / constants.au # au
    state = wrapper.spkezr('1000012', tbd, 'J2000', 'LT+S', '399')
    delta = np.linalg.norm(state[0][:3]) * 1e3 / constants.au # au
    g_factor =  f(rh, rhdot)/rh**2 * 1e-13
    print(rh, delta, rhdot, g_factor)

    # plt.imshow(spec.spec2d, origin='lower')
    # find and trace spectrum
    # nsteps=7 is required because of low S/N
    # trace = pydis.ap_trace(spec.spec2d, nsteps=7, interac=False,
    # display=False)

    # scale lengths
    lp = qcomet.l_scaled(1.3e7, rh) # m
    ld = qcomet.l_scaled(2.1e8, rh) # m
    # expansion velocity
    vd = vp = .85e3/np.sqrt(rh) # m/s
    # total number of molecules in the coma
    # ntotal = dblquad(qcomet.haserd2_int, 0., 1e12,
    #         lambda rho: -1e12, lambda rho: 1e12,
    #         args=(vd, 1., lp, ld))
    ntotal = (481809.33989997394, 451.76173681544606)
    # mean lifetime of daugher molecule
    taud = ld/1e3 # s
    haser_f = []

    conn = sqlite3.connect('67P.db')
    c = conn.cursor()
    # c.execute("INSERT INTO nhaser VALUES (?, ?)", (0, ntotal))
    for ap in [4, 7, 14, 20][0:1]:
        # sky subtraction should be at around 30 pixels from source
        flux_x, sky, fluxerr = pydis.ap_extract(spec.spec2d[50:], trace,
                            apwidth=ap, skysep=3, skywidth=7, skydeg=0)
        ffinal, efinal = pydis.ApplyFluxCal(spec.freq, flux_x, fluxerr,
                            stand.freq, stand.sensfunc)
        flux = background(spec.freq[100:250], ffinal[100:250])
        # number of molecules
        M = 2.812E27 * delta * delta * flux / g_factor
        # Area
        er = equiv_radius((2*ap + 1)*pixel) # arcsec
        radius = delta * constants.au *  er * constants.arcsec * 1e2 # cm
        area = np.pi * (radius)**2
        # Column density
        N = M/area # cm-2
        # number of molecules betweeh 0 and Rho[i]
        # nrho = dblquad(qcomet.haserd2_int, 0., radius*1e-2,
        #     lambda rho: -1e12, lambda rho: 1e12,
        #     args=(vp, 1., lp, ld))
        nrho = (2474.3471272908237, 1.8593880515750243)
        # c.execute("INSERT INTO nhaser VALUES (?, ?)", (ap, nrho))
        haser_fraction = nrho[0]/ntotal[0]
        haser_f.append(haser_fraction)
        mtotal = M/haser_fraction
        q = mtotal/taud
        print(ap, q)
        q_list.append(q)
        date.append(datetime.strptime(
            spec.hdus[0].header['DATE-OBS'],
            "%Y-%m-%dT%H:%M:%S"
            ))
    #     plt.plot(efinal[60:])
    conn.commit()
    conn.close()

qerr = np.linspace(.08, 0.2, len(q_list))*q_list[0]

plt.xlabel('Date [UT]')
plt.ylabel(r'Q(CN) [mol s$^-1$]')
plt.errorbar(date, q_list, yerr=qerr, fmt='o')
plt.gca().set_yscale("log", nonposy='clip')
# autoformat date
plt.gcf().autofmt_xdate()
plt.xlim(date[0] - timedelta(days=1), date[-1] + timedelta(days=1))
plt.savefig('LOTUS_QCN.png')
np.savetxt('LOTUS_QCN.txt', np.transpose((date, q_list)), fmt='%s')
