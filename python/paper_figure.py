#!/usr/bin/python

from liverpool import LOTUSdata, LOTUS_stand, LOTUS_object, LOTUS_spec
from os.path import join
import glob
import pydis

data = LOTUSdata()

# standard star
std_file = join(data.datadir, 'LOTUS', 'l_e_20151029_1_1_1_2.fits')
stand = LOTUS_stand(std_file, lofreq=3250, mode='linear', display=True)

files = glob.glob(join(data.datadir, 'LOTUS', data.comet[2]+'.fits'))
bk_file = glob.glob(join(data.datadir, 'LOTUS',
                    data.comet_background[2]+'.fits'))[0]

bk_spec = LOTUS_spec(bk_file, lofreq=3250)
spec = LOTUS_object(files, lofreq=3250, sky=bk_spec)
trace = pydis.ap_trace(spec.spec2d[50:], nsteps=5, interac=False, display=False)

fl = []

for i in [7,15,30]:
    flux_x, sky, fluxerr = pydis.ap_extract(spec.spec2d[50:], trace,
                    apwidth=i, skysep=3, skywidth=7, skydeg=0)
    ffinal, efinal = pydis.ApplyFluxCal(spec.freq, flux_x, fluxerr,
                            stand.freq, stand.sensfunc)
    fl.append(flux_x)
spec.quick_plot(fl)
