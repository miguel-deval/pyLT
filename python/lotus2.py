#!/usr/bin/python

from liverpool import LOTUS_object, LOTUS_data, LOTUS_stand
from os.path import join
import glob
import matplotlib.pyplot as plt
import pydis

CN = 3871
slice_wl = 6000
ap = 20
skysep = 40
skywidth = 20

data = LOTUS_data()

for night in data.comet[-1:]:
    files = glob.glob(night)
    print(files)
    spec = LOTUS_object(files)
    spec.plot_ext()
    slice_px = spec.get_pixel(4000)
    spec.slice_plot(slice_px)
    # spec.extract(ap=ap, skysep=skysep, skywidth=skywidth, skydeg=1,
    #         trace_display=True, subtract=False)
    # spec.fluxcal(stand_spec=stand)
    # slice_px = spec.get_pixel(slice_wl)
    # spec.slice_plot(slice_px, trace=spec.trace[slice_px], ap=ap, skysep=skysep,
    #         skywidth=skywidth)
    # spec.quick_plot()
