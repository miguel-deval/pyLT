#!/usr/bin/python

from liverpool import SPRAT_object, SPRAT_data, SPRAT_stand
from os.path import join
import glob
import matplotlib.pyplot as plt
import pydis

CN = 3871
slice_wl = 6000
ap = 20
skysep = 40
skywidth = 20

# standard stars
for f in SPRAT_data.standard[:1]:
    stand = SPRAT_stand(f)
    stand.extract(ap=6, skysep=20, skywidth=7)
    stand.fluxcal()
    slice_px = stand.get_pixel(slice_wl)
    stand.slice_plot(slice_px, trace=stand.trace[slice_px], ap=6, skysep=20,
            skywidth=7)
    stand.quick_plot()

# for f in SPRAT_data.solar_analog:
#     solar = SPRAT_object(f)

for night in SPRAT_data.comet:
    files = glob.glob(night[1])
    print(files)
    try:
        spec = SPRAT_object(files)
    except ValueError:
        continue
    spec.extract(ap=ap, skysep=skysep, skywidth=skywidth, skydeg=1,
            subtract=True)
    spec.fluxcal(stand_spec=stand)
    slice_px = spec.get_pixel(slice_wl)
    spec.write_ascii()
    # spec.slice_plot(slice_px, trace=spec.trace[slice_px], ap=ap, skysep=skysep,
    #         skywidth=skywidth)
    # spec.quick_plot()
